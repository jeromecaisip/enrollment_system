from django.core.exceptions import ValidationError
from django.db import models

from teachers.models import Teacher


# Create your models here.

class Subject(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=10)
    is_exclusive = models.BooleanField(default=False)
    exclusive_for_courses = models.ManyToManyField('Course', related_name='eclusive_subjects')
    allowed_teachers = models.ManyToManyField(to=Teacher, related_name='subjects')

    # To add assigned faculties
    # teacher.subjects.add(s)
    def __str__(self):
        return "{1}: {0}".format(self.name, self.code)


class Course(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=10)

    def __str__(self):
        return "{0}".format(self.code)

    @property
    def number_of_students(self):
        from students.models import Student
        number_of_students_enrolled = Student.objects.filter(year_level__course=self).count()
        return number_of_students_enrolled

    @property
    def number_of_teachers(self):
        number_of_teachers = Teacher.objects.filter(subjects__year_levels__course=self).count()
        return number_of_teachers


class SchoolClass(models.Model):
    subject = models.OneToOneField(Subject, on_delete=models.CASCADE, primary_key=True)
    name = models.CharField(max_length=100)
    max_capacity = models.IntegerField()
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE, null=True, related_name='classes')

    def __str__(self):
        return "{1}: {0}".format(self.name, self.subject)

    def clean(self):
        if self.students.count() >= self.max_capacity:
            raise ValidationError('Sorry but this class is already full.')

    def save(self, *args, **kwargs):
        self.full_clean(exclude=['teacher'])
        super(SchoolClass, self).save(*args, **kwargs)

    @property
    def slots_taken(self):
        # Prevent circular dependency
        from students.models import Student
        number_of_students = Student.objects.filter(classes=self).count()
        return number_of_students
