from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def validate_course_exclusivity(value):
    if not value.is_exclusive:
        raise ValidationError(
            _('%(value) Should be a course exclusive subject'),
            params={'value': value}, )
