from faker import Faker
from model_mommy.recipe import Recipe

from .models import Subject, Course, SchoolClass

fake = Faker()
from itertools import cycle

course_names = ['Electronics Engineering', 'Electrical Engineering', 'Computer Engineering']
course_codes = ['ECE', 'EE', 'CPE']
course = Recipe(Course,
                name=cycle(course_names),
                code=cycle(course_codes)
                )

subject = Recipe(Subject,
                 is_exclusive=False,
                 )



