from django.contrib import admin
from .models import Course, Subject, SchoolClass

# Register your models here.

admin.site.register([Subject, Course, SchoolClass])
