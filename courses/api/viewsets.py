from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from students.api.serializers import StudentSerializer
from .serializers import SchoolClassSerializer, SubjectSerializer, CourseSerializer
from ..models import SchoolClass, Subject, Course


class SchoolClassViewSet(viewsets.ModelViewSet):
    serializer_class = SchoolClassSerializer
    queryset = SchoolClass.objects.all()

    @action(methods=['GET'], detail=True)
    def get_students(self, request):
        obj = self.get_object()
        qs = obj.students.all()
        serializer = StudentSerializer(qs, many=True, context={'request': request})
        return Response(serializer.data, 200)

    def perform_update(self, serializer):
        serializer.save()
        # Delegate message sending to the background task synchronously without blocking instead of async.
        channel_layer = get_channel_layer()
        school_class = self.get_object()
        async_to_sync(channel_layer.send)('class-update', {'type': 'send_update',
                                                           'school_class': SchoolClassSerializer(school_class, context={
                                                               'request': self.request}).data})


class SubjectViewSet(viewsets.ModelViewSet):
    serializer_class = SubjectSerializer
    queryset = Subject.objects.all()


class CourseViewSet(viewsets.ModelViewSet):
    serializer_class = CourseSerializer
    queryset = Course.objects.all()

    def get_queryset(self):
        subject_id = self.request.query_params.get('subject_id', None)
        if subject_id:
            Course.objects.filter(year_levels__required_subjects__id=subject_id)
        else:
            return self.queryset
