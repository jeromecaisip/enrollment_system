from rest_framework import serializers

from ..models import SchoolClass, Subject, Course


class SchoolClassSerializer(serializers.ModelSerializer):
    class Meta:
        model = SchoolClass
        fields = ['id', 'subject', 'name', 'max_capacity', 'teacher', 'url', 'students', 'slots_taken']

    id = serializers.ReadOnlyField(source='pk')
    subject = serializers.PrimaryKeyRelatedField(queryset=Subject.objects.all())
    url = serializers.HyperlinkedIdentityField('api:schoolclass-detail')


class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = '__all__'


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ['name', 'number_of_students', 'number_of_teachers']
