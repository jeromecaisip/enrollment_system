from itertools import cycle

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from model_mommy import mommy

from courses.models import SchoolClass, Subject, Course
from students.models import Student
from teachers.models import Teacher


class Command(BaseCommand):
    help = 'Generate YearLevel objects'

    def handle(self, *args, **options):
        if Teacher.objects.count() <= 10:
            mommy.make_recipe('teachers.teacher', _quantity=10)
        else:
            print('Teacher data is enought')
        if Student.objects.count() <= 10:
            mommy.make_recipe('students.student', _quantity=50)
            mommy.make_recipe('students.year_level', _quantity=21)
        else:
            print('Student data is enough')
        if Course.objects.count() <= 2:
            mommy.make_recipe('courses.subject', _quantity=20)
        else:
            print('Course data is enought')

        subjects = [x.__str__()[10] for x in Subject.objects.all()]

        mommy.make(SchoolClass,
                   max_capacity=50,
                   name=cycle(subjects),
                   _quantity=len(subjects), )

        if not User.objects.filter(is_superuser=True).exists():
            User.objects.create_superuser('admin', '', 'admin123')
