from djongo import models
from django.contrib.auth.models import User


class UserProfile(models.Model):
    TYPE_CHOICES = [
        (1, 'admin'),
        (2, 'teacher'),
        (3, 'student')
    ]
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    type = models.IntegerField(choices=TYPE_CHOICES, )

    def __str__(self):
        return "{0}:{1}".format(self.user.username,UserProfile.TYPE_CHOICES[self.type-1][1])