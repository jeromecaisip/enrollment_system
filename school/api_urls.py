from rest_framework import routers

from courses.api.viewsets import SubjectViewSet, SchoolClassViewSet, CourseViewSet
from students.api.viewsets import StudentViewSet
from teachers.api.viewsets import TeacherViewSet

app_name = 'api'

router = routers.DefaultRouter()
router.register('students', StudentViewSet)
router.register('teachers', TeacherViewSet)
router.register('subjects', SubjectViewSet)
router.register('classes', SchoolClassViewSet)
router.register('courses', CourseViewSet)

urlpatterns = router.urls
