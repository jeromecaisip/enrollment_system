from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter, ChannelNameRouter

from class_enrollment.channel_routing import websocket_urlpatterns as class_websocket_urlpatterns
from class_enrollment.consumers import ClassUpdateConsumer

application = ProtocolTypeRouter({
    # (http->django views is added by default)
    'websocket': AuthMiddlewareStack(
        URLRouter(
            class_websocket_urlpatterns
        )
    ),
    'channel': ChannelNameRouter({
        'class-update': ClassUpdateConsumer,
    })
})
