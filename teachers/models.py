from djongo import models
from core.models import UserProfile


class Teacher(models.Model):
    profile = models.OneToOneField(UserProfile, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

    def __str__(self):
        return "{0} {1}".format(self.first_name, self.last_name)

    @property
    def allowed_subjects(self):
        from courses.models import Subject
        return Subject.objects.filter(allowed_teachers=self)
