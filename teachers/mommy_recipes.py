from faker import Faker
from model_mommy.recipe import Recipe

from .models import Teacher

fake = Faker()
from itertools import cycle

first_names = [fake.first_name() for x in range(10)]
last_names = [fake.last_name() for x in range(10)]
teacher = Recipe(Teacher,
                 first_name=cycle(first_names),
                 last_name=cycle(last_names)
                 )
