from rest_framework import viewsets
from .serializers import TeacherSerializer

from ..models import Teacher
from courses.models import Subject


class TeacherViewSet(viewsets.ModelViewSet):
    serializer_class = TeacherSerializer
    queryset = Teacher.objects.all()

    def get_queryset(self):
        subject_id = self.request.query_params.get('subject_id', None)
        if subject_id:
            return Teacher.objects.filter(subjects__id=subject_id)
        else:
            return self.queryset
