from ..models import Teacher
from rest_framework import serializers
from courses.models import Subject


class AllowedSubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = ['id', 'name', 'code']


class TeacherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Teacher
        fields = ('first_name', 'last_name', 'url', 'allowed_subjects')

    url = serializers.HyperlinkedIdentityField(view_name='api:teacher-detail')
    allowed_subjects = AllowedSubjectSerializer(many=True)
