from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from core.models import UserProfile
from courses.models import Course, SchoolClass, Subject


class YearLevel(models.Model):
    YEAR_IN_SCHOOL_CHOICES = (
        (1, 'First year'),
        (2, 'Second year'),
        (3, 'Third year'),
        (4, 'Fourth year'),
        (5, 'Fifth year'),
        (6, 'Sixth year'),
        (7, 'Seventh year'),
    )

    year_level = models.IntegerField(choices=YEAR_IN_SCHOOL_CHOICES,
                                     validators=[MaxValueValidator(7), MinValueValidator(1)])
    required_subjects = models.ManyToManyField(to=Subject, related_name='year_levels')
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='year_levels')

    def __str__(self):
        return "{1} {0}".format(YearLevel.YEAR_IN_SCHOOL_CHOICES[self.year_level - 1][1], self.course)


class Student(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    profile = models.OneToOneField(UserProfile, on_delete=models.CASCADE)
    year_level = models.ForeignKey(YearLevel, related_name='students', on_delete=models.DO_NOTHING, null=True,
                                   blank=True)
    classes = models.ManyToManyField(SchoolClass, related_name='students')

    def __str__(self):
        return "{0} {1}".format(self.first_name, self.last_name)
