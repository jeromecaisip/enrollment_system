from ..models import YearLevel, Student
from rest_framework import serializers


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ['first_name', 'last_name', 'year_level', 'course', 'classes', 'url']

    course = serializers.ReadOnlyField(source='year_level.course.code')
    url = serializers.HyperlinkedIdentityField(view_name='api:student-detail')
    year_level = serializers.PrimaryKeyRelatedField(source='year_level.year_level',
                                                    queryset=YearLevel.objects.order_by('year_level'))

    classes = serializers.StringRelatedField(many=True)

    # TODO study about dynamic serializer fields to determine queryset based on request


class YearLevelSerializer(serializers.ModelSerializer):
    class Meta:
        model = YearLevel
        fields = '__all__'
