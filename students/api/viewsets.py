from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from courses.api.serializers import SchoolClassSerializer
from courses.api.serializers import SubjectSerializer
from courses.models import SchoolClass
from courses.models import Subject
from teachers.models import Teacher
from teachers.api.serializers import TeacherSerializer
from .serializers import StudentSerializer
from ..models import Student


class StudentViewSet(viewsets.ModelViewSet):
    serializer_class = StudentSerializer
    queryset = Student.objects.all()

    @action(methods=['PATCH', 'PUT'], detail=True)
    def add_to_class(self, request, *args, **kwargs):
        class_id = request.data['class_id']
        class_obj = SchoolClass.objects.get(pk=class_id)
        student = self.get_object()
        student.classes.add(class_obj)
        serializer = StudentSerializer(student, context={'request': request})

        return Response(serializer.data, status=200)

    @action(methods=['PATCH', 'PUT'], detail=True)
    def remove_from_class(self, request, *args, **kwargs):
        class_id = request.data['class_id']
        class_obj = SchoolClass.objects.get(pk=class_id)
        student = self.get_object()
        student.classes.remove(class_obj)
        serializer = StudentSerializer(student, context={'request': request})

        return Response(serializer.data, status=200)

    @action(methods=['GET'], detail=True)
    def get_classes(self, request, *args, **kwargs):
        student = self.get_object()
        serializer = SchoolClassSerializer(student.classes, many=True, context={'request': request})
        return Response(serializer.data, status=200)

    @action(methods=['GET'], detail=True)
    def required_subjects(self, request, *args, **kwargs):
        obj = self.get_object()
        required_subjects = Subject.objects.filter(year_levels__course=obj.year_level.course)
        serializer = SubjectSerializer(required_subjects, many=True)
        return Response(serializer.data, status=200)

    @action(methods=['GET'], detail=True)
    def get_profs(self, request, *args, **kwargs):
        student = self.get_object()
        profs = Teacher.objects.filter(schoolclass__students__id=student.id)
        serializer = TeacherSerializer(profs, many=True, context={'request': request})
        return Response(serializer.data, status=200)
