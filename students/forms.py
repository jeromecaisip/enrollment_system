from django import forms
from .models import YearLevel
from courses.models import Subject


class YearLevelForm(forms.ModelForm):

    year_level = forms.ChoiceField(choices=YearLevel.YEAR_IN_SCHOOL_CHOICES)
    required_subjects = forms.ModelMultipleChoiceField(Subject.objects.all())




