from faker import Faker
from model_mommy.recipe import Recipe, foreign_key

from .models import Student, YearLevel
from courses.models import Course
from courses.mommy_recipes import course

fake = Faker()
from itertools import cycle

first_names = [fake.first_name() for x in range(50)]
last_names = [fake.last_name() for x in range(50)]

student = Recipe(Student,
                 first_name=cycle(first_names),
                 last_name=cycle(last_names)
                 )

year_levels = [x + 1 for x in range(7)]

year_level = Recipe(YearLevel,
                    year_level=cycle(year_levels),
                    course=foreign_key(course)
                    )
