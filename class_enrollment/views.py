from django.shortcuts import render
from courses.models import SchoolClass

# Create your views here.


def index(request):
    qs = SchoolClass.objects.all()
    return render(request, 'class_enrollment/index.html', {'classes':qs})
