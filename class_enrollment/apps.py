from django.apps import AppConfig


class ClassEnrollmentConfig(AppConfig):
    name = 'class_enrollment'
