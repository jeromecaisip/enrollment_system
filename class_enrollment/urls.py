from django.urls import path

from . import views

app_name = 'class_enrollment'

urlpatterns = [
    path('', views.index, name='enrollment_index')
]
