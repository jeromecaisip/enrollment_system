from django.urls import path

from . import consumers

websocket_urlpatterns = [
    path('ws/<int:class_id>/', consumers.ClassConsumer),
    path('ws/enrollment/', consumers.ClassEnrollmentConsumer)
]
