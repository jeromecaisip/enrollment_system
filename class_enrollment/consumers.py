import json

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer, SyncConsumer
from channels.layers import get_channel_layer


# Responsible for background task processing
class ClassUpdateConsumer(SyncConsumer):
    def send_update(self, message):
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)("enrollment",
                                                {"type": "slot_update", "message": message['school_class']})

# Responsible for websocket handling
class ClassEnrollmentConsumer(WebsocketConsumer):
    def connect(self):
        self.group_name = self.scope.get('path').split('/')[2]
        async_to_sync(self.channel_layer.group_add)(
            self.group_name,
            self.channel_name
        )
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.group_name,
            self.channel_name
        )

    def slot_update(self, event):
        # Send message to WebSocket
        self.send(text_data=json.dumps(
            event['message']
        ))


class ClassConsumer(WebsocketConsumer):
    def connect(self):
        self.accept()

    def disconnect(self, close_code):
        pass

    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        self.send(text_data=json.dumps({
            'message': message
        }))
